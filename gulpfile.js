const gulp = require('gulp')
const nunjucksRender = require('gulp-nunjucks-render')
const htmlmin = require('gulp-htmlmin')

gulp.task('nunjucks', function() {
  return gulp.src('src/pages/**/*.+(html|njk)')
    .pipe(nunjucksRender({
      path: ['src/templates']
    }))
    .pipe(htmlmin({ collapseWhitespace: true }))
    .pipe(gulp.dest('out/'))
})
