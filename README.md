To generate the nav menu HTML and CSS:

`npm run compile`

Then copy the contents of `out/tailwind.css` into Kentico’s `sitestyle` CSS file, and the contents of the `out/*.html` files into their respective navigation menu pages.